#include <Wire.h>

#define TCAADDR 0x70
#include <Adafruit_BMP085.h>
#include <Adafruit_L3GD20.h>

// I2C ports

// BMP sensors
Adafruit_BMP085 bmp1;
//int ports_bmp[3] = {1,2,7};
int port_bmp1 = 1;
Adafruit_BMP085 bmp2;
int port_bmp2 = 2;
Adafruit_BMP085 bmp3;
int port_bmp3 = 7;

// Gyro
Adafruit_L3GD20 gyro;
int port_gyro = 8;

int delay_temp_reading = 2000;

int uncertainty_bmp_pressure = 2;

int iniciated_bmp_sensor[3] = {1, 1, 1};
int calibrated_bmp_sensor[3] = {1, 1, 1};

int iniciated_gyro = 1;

void tcaselect(uint8_t i) {
  if (i > 7) return;

  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();
}


void setup(void)
{
  Serial.begin(9600);

  /* Initialise the 1st sensor */
  tcaselect(port_bmp1);

  if (!bmp1.begin()) {
    Serial.println("Could not find a valid BMP085/BMP180 1 sensor, check wiring!");
    iniciated_bmp_sensor[0] = 0;

  }
  tcaselect(port_bmp2);

  if (!bmp2.begin()) {
    Serial.println("Could not find a valid BMP085/BMP180 2 sensor, check wiring!");
    iniciated_bmp_sensor[1] = 0;
  }
  tcaselect(port_bmp3);

  if (!bmp3.begin()) {
    Serial.println("Could not find a valid BMP085/BMP180 3 sensor, check wiring!");
    iniciated_bmp_sensor[2] = 0;
  }
  tcaselect(port_gyro);
  if (!gyro.begin(gyro.L3DS20_RANGE_250DPS)) {
    iniciated_gyro = 0;
  }

}
void loop(void)
{



  if (iniciated_gyro) {
    Serial.println("\nReading Gyro");
    tcaselect(port_gyro);
    gyro.read();
    print_gyro_result(gyro);
  }

  float results_sensor1[5] = {0, 0, 0, 0, 0};
  if (iniciated_bmp_sensor[0]) {
    Serial.println("\nReading Sensor 1");
    tcaselect(port_bmp1);
    readSensor(bmp1, results_sensor1);
    print_result_bmp(results_sensor1);
  }



  float results_sensor2[5] = {0, 0, 0, 0, 0};
  if (iniciated_bmp_sensor[1]) {
    Serial.println("\nReading Sensor 2");
    tcaselect(port_bmp2);
    readSensor(bmp2, results_sensor2);
    print_result_bmp(results_sensor2);
  }


  float results_sensor3[5] = {0, 0, 0, 0, 0};
  if (iniciated_bmp_sensor[2]) {
    Serial.println("\nReading Sensor 3");
    tcaselect(port_bmp3);
    readSensor(bmp3, results_sensor3);
    print_result_bmp(results_sensor3);
  }

  Serial.println("\nCOMPUTING MEAN");
  int number_sensores_bmp = 3;
  float temperatures_measure[3] = {results_sensor1[0], results_sensor2[0], results_sensor3[0]};
  float pressure_measures[3] = {results_sensor1[1], results_sensor2[1], results_sensor3[1]};
  float altitude_measures[3] = {results_sensor1[2], results_sensor2[2], results_sensor3[2]};
  float sea_level_measures[3] = {results_sensor1[3], results_sensor2[3], results_sensor3[3]};
  float tmp_mean_temperature = compute_mean(temperatures_measure, iniciated_bmp_sensor, number_sensores_bmp);

  float max_value = 0;

  for (int i = 0; i < number_sensores_bmp; i++) {
    float dif_temperature = abs(temperatures_measure[i] - tmp_mean_temperature);
    if (dif_temperature > uncertainty_bmp_pressure ) {
      if (max_value < dif_temperature) {
        max_value = dif_temperature;
        calibrated_bmp_sensor[i] = 0;

      }
    }

  }
  float  mean_temperature = compute_mean(temperatures_measure, calibrated_bmp_sensor, number_sensores_bmp);
  float  mean_pressure = compute_mean(pressure_measures, calibrated_bmp_sensor, number_sensores_bmp);
  float  mean_altitude = compute_mean(altitude_measures, calibrated_bmp_sensor, number_sensores_bmp);
  float  mean_sea_level = compute_mean(sea_level_measures, calibrated_bmp_sensor, number_sensores_bmp);
  Serial.println(" ##################################### MEANs ##################################### ");
  Serial.print("temp: ");
  Serial.println(mean_temperature);
  Serial.print("pressure: ");
  Serial.println(mean_pressure);
  Serial.print("altitude: ");
  Serial.println(mean_altitude);
  Serial.print("sea level: ");
  Serial.println(mean_sea_level);
  delay(delay_temp_reading);


}
float compute_mean(float* results, int* use_sensor, int len) {
  float sum = 0L;
  int rejected_sensors = 0;
  for (int i = 0; i < len; i++) {
    if (use_sensor[i]) {
      sum += results[i];
    }
    else {
      rejected_sensors ++;
    }
  }
  if (rejected_sensors != len) {

    return ((float) sum) * 1.0 / (len - rejected_sensors);
  }
  else {
    return 0;
  }
}


void print_gyro_result(Adafruit_L3GD20 gyro) {
  Serial.print("X: "); Serial.print((int)gyro.data.x);   Serial.print(" ");
  Serial.print("Y: "); Serial.print((int)gyro.data.y);   Serial.print(" ");
  Serial.print("Z: "); Serial.println((int)gyro.data.z); Serial.print(" ");

}
void print_result_bmp(float results[]) {

  Serial.print("Temperature = ");
  Serial.print(results[0]);
  Serial.println(" *C");

  Serial.print("Pressure = ");
  Serial.print(results[1]);
  Serial.println(" Pa");

  Serial.print("Altitude = ");
  Serial.print(results[2]);
  Serial.println(" meters");

  Serial.print("Pressure at sealevel (calculated) = ");
  Serial.print(results[3]);
  Serial.println(" Pa");
  Serial.print("Real altitude = ");
  Serial.print(results[4]);
  Serial.println(" meters");


}


void readSensor(Adafruit_BMP085 bmp, float results[]) {


  results[0] = bmp.readTemperature();
  results[1] = bmp.readPressure();
  results[2] = bmp.readAltitude();
  results[3] = bmp.readSealevelPressure();

  // you can get a more precise measurement of altitude
  // if you know the current sea level pressure which will
  // vary with weather and such. If it is 1015 millibars
  // that is equal to 101500 Pascals.
  results[4] = bmp.readAltitude(101500);

}

