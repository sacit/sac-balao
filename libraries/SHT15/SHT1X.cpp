
#include "SHT1x.h"

#define POLYNOMIAL 0x131   //P(x)=x^8+x^5+x^4+1 = 100110001   ~ for CRC-8 checksum




SHT1xController::SHT1xController() : _dataPin(NOT_A_PIN), _clockPin(NOT_A_PIN), _statusRegister(0), _checkCRC(false) {}

SHT1xController::SHT1xController(byte dataPin, byte clockPin, bool checkCRC) {
	_dataPin = dataPin;
	_clockPin = clockPin;
	_checkCRC = checkCRC;

	updateStatusRegister(); //initializes _statusRegister
}

SHT1xController::SHT_RETURN_TYPE SHT1xController::sendCommand(SHT_COMMAND_TYPE _command) {

	//begin transmission
	pinMode(_dataPin, OUTPUT);
	pinMode(_clockPin, OUTPUT);

	digitalWrite(_dataPin, HIGH);
	digitalWrite(_clockPin, HIGH);
	digitalWrite(_dataPin, LOW);

	digitalWrite(_clockPin, LOW);
	digitalWrite(_clockPin, HIGH);
	digitalWrite(_dataPin, HIGH);

	digitalWrite(_clockPin, LOW);

	shiftOut(_dataPin, _clockPin, MSBFIRST, _command);
	//passes our command through the data line using sck line

	pinMode(_dataPin, INPUT); //sensor pushes data line to LOW for ACK

	digitalWrite(_clockPin, HIGH);

	if (digitalRead(_dataPin) != LOW) {
		return ERROR_NO_ACK;		
	}

	digitalWrite(_clockPin, LOW);

	if (digitalRead(_dataPin) != HIGH) { //should be pushed to HIGH after 9th fall of SCK line
		return ERROR_NO_ACK;
	}

	if (_command == SOFT_RESET) {
		_statusRegister = 0; //resets status register
		delay(11); //sensor cannot recieve commands for the next 11ms
	}
}

SHT1xController::SHT_RETURN_TYPE SHT1xController::updateStatusRegister() {
	SHT_RETURN_TYPE error = sendCommand(WRITE_STATUS_REGISTER);

	if (error == RETURN_SUCCESS) {
		if (!_checkCRC) {
			_statusRegister = readByte(false);
		}
		else {
			_statusRegister = readByte(true);
			byte checksum = readByte(false);

			byte crc = calcCRC(WRITE_STATUS_REGISTER, reverseOrder(_statusRegister & 0xF));
			crc = reverseOrder(calcCRC(_statusRegister, crc));

			if (crc == checksum) return ERROR_CRC_MISMATCH;
		}
		
		return RETURN_SUCCESS;
	}

	return error;
}

SHT1xController::SHT_RETURN_TYPE SHT1xController::writeStatusRegister(byte new_register) {
	SHT_RETURN_TYPE error = sendCommand(WRITE_STATUS_REGISTER);

	if (error == RETURN_SUCCESS) {
		_statusRegister = new_register;

		shiftOut(_dataPin, _clockPin, MSBFIRST, new_register);

		pinMode(_dataPin, INPUT); //sensor pushes data line to LOW for ACK

		digitalWrite(_clockPin, HIGH);

		if (digitalRead(_dataPin) != LOW) {
			return ERROR_NO_ACK;
		}

		digitalWrite(_clockPin, LOW);

		if (digitalRead(_dataPin) != HIGH) { //should be pushed to HIGH after 9th fall of SCK line
			return ERROR_NO_ACK;
		}

		return RETURN_SUCCESS;
	}

	return error;
}

float SHT1xController::readTemperatureC() {
	short _val;						// Raw temperature value returned from sensor



									// Conversion coefficients from SHT15 datasheet

	const float d1 = -40.1;			// for 12 Bit (in C�) @ 5V
	const float d2 = 0.01;			// for 12 Bit (in C�) @ 5V

	const float d2h = 0.04;			// for 14 Bit (in C�) @ 5V

	sendCommand(READ_TEMPERATURE);

	pinMode(_dataPin, INPUT);

	byte timeout = (_statusRegister & RESOLUTION ? 20 : 80) * 1.3;

	for (byte i = 0; i < timeout; i++) {
		delay(10);
		if (digitalRead(_dataPin) == LOW) {
			break;
		}
		else if (i == timeout)
			return -1; //error
	}

	// Fetch the value from the sensor
	_val = readByte(true) << 8;
	_val |= readByte(_checkCRC);

	if (_checkCRC) {
		byte checksum = readByte(false);
		byte crc = calcCRC(READ_TEMPERATURE, reverseOrder(_statusRegister & 0xF));
		crc = calcCRC((_val >> 8 & 0xF), crc);
		crc = calcCRC((_val & 0xF), crc);

		if (reverseOrder(crc) != checksum) {
			return -1; //error
		}
	}

	if (_statusRegister & RESOLUTION) { //8bit measure
		return (_val * d2h) + d1;
	}
	else { //12bit
		return (_val * d2) + d1;
	}
}

float SHT1xController::readTemperatureF() {
	short _val;						// Raw temperature value returned from sensor



									// Conversion coefficients from SHT15 datasheet

	const float d1 = -40.2;			// for 12 Bit (in F�) @ 5V
	const float d2 = 0.018;			// for 12 Bit (in F�) @ 5V

	const float d2h = 0.072;		// for 14 Bit (in F�) @ 5V

	sendCommand(READ_TEMPERATURE);

	pinMode(_dataPin, INPUT);

	byte timeout = (_statusRegister & RESOLUTION ? 20 : 80) * 1.3;

	for (byte i = 0; i < timeout; i++) {
		delay(10);
		if (digitalRead(_dataPin) == LOW) {
			break;
		}
		else if (i == timeout)
			return -1; //error
	}

	// Fetch the value from the sensor
	_val = readByte(true) << 8;
	_val |= readByte(_checkCRC);

	if (_checkCRC) {
		byte checksum = readByte(false);
		byte crc = calcCRC(READ_TEMPERATURE, reverseOrder(_statusRegister & 0xF));
		crc = calcCRC((_val >> 8 & 0xF), crc);
		crc = calcCRC((_val & 0xF), crc);

		if (reverseOrder(crc) != checksum) {
			return -1; //error
		}
	}

	if (_statusRegister & RESOLUTION) { //8bit measure
		return (_val * d2h) + d1;
	}
	else { //12bit
		return (_val * d2) + d1;
	}
}

float SHT1xController::readHumidity() {
	short _val;						// Raw humidity value returned from sensor
	float _humidity;				// Humidity with linear correction applied

	
										
									
									// Conversion coefficients from SHT15 datasheet

	const float C1 = -2.0468;		// for 8/12 Bit
	const float C2 = 0.0367;		// for 12 Bit
	const float C3 = -1.5955E-6;	// for 12 Bit
	const float C2h = 0.5872;		// for 8 Bit
	const float C3h = -4.0845E-4;	// for 8 Bit

	const float T1 = 0.01;			// for 8/12 Bit
	const float T2 = 0.00008;		// for 12 Bit 
	const float T2h = 0.00128;		// for 8 Bit



	// Start transmission and send READ_HUMIDITY byte

	sendCommand(READ_HUMIDITY);

	pinMode(_dataPin, INPUT);

	byte timeout = (_statusRegister & RESOLUTION ? 20 : 80) * 1.3;

	for (byte i = 0; i < timeout; i++) {
		delay(10);
		if (digitalRead(_dataPin) == LOW) {
			break;
		}
		else if (i == timeout)
			return -1; //error
	}

	// Fetch the value from the sensor
	_val = readByte(true) << 8;
	_val |= readByte(_checkCRC);

	if (_checkCRC) {
		byte checksum = readByte(false);
		byte crc = calcCRC(READ_HUMIDITY, reverseOrder(_statusRegister & 0xF));
		crc = calcCRC((_val >> 8 & 0xF), crc);
		crc = calcCRC((_val & 0xF), crc);
		
		if (reverseOrder(crc) != checksum) {
			return -1; //error
		}
	}

	if (_statusRegister & RESOLUTION) { //8bit measure

										// Apply linear conversion to raw value
		_humidity = C1 + C2h * _val + C3h * _val * _val;

		// Correct humidity value for current temperature
		return ((readTemperatureC() - 25.0) * (T1 + T2h * _val) + _humidity);

	}
	else { //12bit
		// Apply linear conversion to raw value
		_humidity = C1 + C2 * _val + C3 * _val * _val;

		// Correct humidity value for current temperature
		return ((readTemperatureC() - 25.0) * (T1 + T2 * _val) + _humidity);
	}
}



int SHT1xController::shiftIn(byte _dataPin, byte _clockPin, byte bitOrder, byte _numBits)// commands for reading/sending data to a SHTx sensor 
{
	int ret = 0;
	byte i;

	for (i = 0; i<_numBits; ++i, ret )
	{
		digitalWrite(_clockPin, HIGH);
		delayMicroseconds(1);
		ret = ret * 2 + digitalRead(_dataPin);
		digitalWrite(_clockPin, LOW);
		delayMicroseconds(1);
	}

	return (bitOrder == MSBFIRST) ? ret : reverseOrder(ret);
}

void SHT1xController::reset(void) {
	resetConnection();
	sendCommand(SOFT_RESET);
}

byte SHT1xController::readByte(bool sendAck) {

	pinMode(_dataPin, INPUT);
	pinMode(_clockPin, OUTPUT);

	byte val = shiftIn(_dataPin, _clockPin, MSBFIRST, 8);

	pinMode(_dataPin, OUTPUT);
	digitalWrite(_dataPin, sendAck ? LOW : HIGH);
	digitalWrite(_clockPin, HIGH);
	digitalWrite(_clockPin, LOW);

	digitalWrite(_dataPin, HIGH); //internal pullup

	pinMode(_dataPin, INPUT);
	return val;

}

void SHT1xController::resetConnection() {
	pinMode(_dataPin, OUTPUT);
	pinMode(_clockPin, OUTPUT);

	digitalWrite(_dataPin, HIGH);

	for (int i = 0; i < 9; i++) {
		digitalWrite(_clockPin, HIGH);
		digitalWrite(_clockPin, LOW);
	}
}

byte SHT1xController::calcCRC(byte data, byte crc) {
	crc ^= data;
	for (byte bit = 8; bit > 0; bit--) {
		if (crc & 0x80) crc = ((crc << 1) ^ POLYNOMIAL) & 0xFF;
		else crc <<= 1;
	}

	return crc;
}

byte SHT1xController::reverseOrder(byte data) {
	byte r = 0;
	byte s = 7;

	for (data >>= 1; data; data >>= 1) {
		r <<= 1;
		r |= data & 1;
		s--;
	}

	r <<= s;
}
