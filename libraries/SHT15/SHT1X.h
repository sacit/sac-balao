/**
* SHT1x Expanded Library
* 
* Based in the library created by Sparkfun.
* SAC_IT 
*
*
* Beerware is funware!
*
* Summary:
*	Manages communication with SHT1x series (SHT10, SHT11, SHT15)
*	temperature / humidity sensors from Sensirion (www.sensirion.com).
*
* Notes:
*	This library is an expansion of the SHT1x. It provides a usage of the status register applications,
*	which allow for an extended usage of the sensor, thefore it supports both 8/12bit and 12/16bit data measurements.
*	It also provides an optional CRC-8 checksum calculation to verify data transmissions.
*/

#ifndef SHT1X_H //header guard
#define SHT1X_H

#if defined(ARDUINO) && (ARDUINO >= 100) // fixes compatibility issues with outdated Arduino IDE's.
#include <Arduino.h>
#else
#include <WProgram.h>
#endif

class SHT1xController{
private:

	//function return types
	enum SHT_RETURN_TYPE : byte {
		RETURN_SUCCESS = 0,					// Indicates successful exit.
		ERROR_NO_ACK = 1,					// Failure to acknowledge data from sensor
		ERROR_CRC_MISMATCH = 2				// Transmission data corruption
	};

	//list of sht1x commands
	enum SHT_COMMAND_TYPE : byte {			//		Command description					ADDRESS | COMMAND
		READ_TEMPERATURE = 0x03,			// Measure temperature command.					[000|00011]
		READ_HUMIDITY = 0x05,				// Measure relative humidity command.			[000|00101]
		READ_STATUS_REGISTER = 0x07,		// Read status register command.				[000|00111]
		WRITE_STATUS_REGISTER = 0x06,		// Write status register command.				[000|00110]
		SOFT_RESET = 0x1E					// Soft reset command.							[000|11110]
	};

	//flags for the status register
	enum SHT_REGISTER_FLAGS : byte {				//		Flags description						
		END_OF_BATTERY = 0x40,				// [READ] Detects voltages below 2.47 V.		[01000000]
		HEATER = 0x04,						// [READ/WRITE] Usage of sensor's heater.		[00000100]
		OTP_RELOAD = 0x02,					// [READ/WRITE] Indicates OTP reload usage.		[00000010]
		RESOLUTION = 0x01,					// [READ/WRITE] Indicates resolution type.		[00000001]
	};

public:
	SHT1xController(void);
	SHT1xController(byte dataPin, byte clockPin, bool checkCRC = false);


	//Reads current temperature in degrees Celsius
	float readTemperatureC();

	//Reads the sensor's temperature Fahrenheit degrees	
	float readTemperatureF();

	//Reads current temperature-corrected relative humidity
	float readHumidity();


public:
	SHT_RETURN_TYPE sendCommand(SHT_COMMAND_TYPE _command);
	SHT_RETURN_TYPE updateStatusRegister();
	SHT_RETURN_TYPE writeStatusRegister(byte new_register);

	byte readByte(bool sendAck);

	void resetConnection(void);
	void reset(void);

private:
	static byte calcCRC(byte data, byte checksum);
	static byte reverseOrder(byte data);
	static int shiftIn(byte _dataPin, byte _clockPin, byte bitOrder, byte _numBits);

protected:
	byte _dataPin;
	byte _clockPin;
	byte _statusRegister;
	bool _checkCRC;
};


#endif//SHT1X_H